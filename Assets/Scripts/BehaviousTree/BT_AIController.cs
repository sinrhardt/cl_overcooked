using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class BT_AIController : MonoBehaviour
{
    BT_Node.Status treeStatus = BT_Node.Status.Running;

    public enum ActionState { Idle, Working };
    ActionState state = ActionState.Idle;

    [SerializeField] private Transform restPosition;
    [SerializeField] private Transform redDepositPosition;
    [SerializeField] private Transform blueDepositPosition;
    [SerializeField] private Transform yellowDepositPosition;
    [SerializeField] private Transform tablePosition;
    [SerializeField] private Transform outPutPosition;
    [SerializeField] private GameObject pot;

    //
    [SerializeField] private TMP_Text redValue;
    [SerializeField] private TMP_Text blueValue;
    [SerializeField] private TMP_Text yellowValue;
    
    NavMeshAgent agent;
    BT_Root treeRoot;

    private GameManager _gameManager;
    private Recipe _currentRecipe;
    
    // Inventory
    private int _redIngredient;
    private int _blueIngredient;
    private int _yellowIngredient;

    private bool _missingRed = false;
    private bool _missingBlue = false;
    private bool _missingYellow = false;
    
    private void Start()
    {
        _gameManager = GameManager.Instance;
        agent = GetComponent<NavMeshAgent>();

        _redIngredient = 5;
        _blueIngredient = 5;
        _yellowIngredient = 5;

        AssignValue();

        treeRoot = new BT_Root();
        // BT_Sequence lootItem = new BT_Sequence("Acquire Item");
        // BT_Leaf getToItem = new BT_Leaf("Get To Item", GetToItem);
        // BT_Leaf escape = new BT_Leaf("Escape", GetToSafety);
        // BT_Leaf goToFrontDoor = new BT_Leaf("Go To FrontDoor", GoToFrontDoor);
        // BT_Leaf goToBackDoor = new BT_Leaf("Go To BackDoor", GoToBackDoor);
        // BT_Selector openDoor = new BT_Selector("Open Door");
        // BT_Leaf canGetToItem = new BT_Leaf("Can Get To Item", CanGetToItem);
        //
        // //Primo layer
        // treeRoot.AddChild(lootItem);
        //
        // //Secondo layer
        // lootItem.AddChild(canGetToItem);
        // lootItem.AddChild(openDoor);
        // lootItem.AddChild(getToItem);
        // lootItem.AddChild(escape);
        //
        // //terzo layer
        // openDoor.AddChild(goToFrontDoor);
        // openDoor.AddChild(goToBackDoor);
        //
        // treeRoot.PrintTree();
        
        BT_Selector behaviour = new BT_Selector("Behaviour");
        BT_Sequence work = new BT_Sequence("Work");
        BT_Leaf rest = new BT_Leaf("Rest", GoToRest);
        //
        BT_Leaf hasWork = new BT_Leaf("HasWork", HasWork);
        BT_Sequence makePot = new BT_Sequence("MakePot");
        //
        BT_Leaf hasOrder = new BT_Leaf("HasOrder", HasOrder);
        BT_Leaf hasResource = new BT_Leaf("HasResource", HasResources);
        BT_Leaf goTable = new BT_Leaf("GoTable", GoToTable);
        BT_Leaf craft = new BT_Leaf("Craft", Craft);
        BT_Leaf goOutput = new BT_Leaf("GoOutput", GoToOutput);
        
        
        // Primo layer
        treeRoot.AddChild(behaviour);
        
        // Secondo layer
        behaviour.AddChild(work);
        behaviour.AddChild(rest);
        
        // Terzo layer
        work.AddChild(hasWork);
        work.AddChild(makePot);
        
        // Quarto layer
        makePot.AddChild(hasOrder);
        makePot.AddChild(hasResource);
        makePot.AddChild(goTable);
        makePot.AddChild(craft);
        makePot.AddChild(goOutput);
        
        treeRoot.PrintTree();

    }

    private void Update()
    {
        if (treeStatus != BT_Node.Status.Success)
            treeStatus = treeRoot.Process();
        else
        {
            treeStatus = BT_Node.Status.Running;
            treeRoot.currentChild = 0;
        }
    }

    private void AssignValue()
    {
        redValue.SetText("" + _redIngredient);
        blueValue.SetText("" + _blueIngredient);
        yellowValue.SetText("" + _yellowIngredient);
    }

    BT_Node.Status GoToLocation(Vector3 destination)
    {
        if (state == ActionState.Idle)
        {
            agent.SetDestination(destination);
            state = ActionState.Working;
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
        {
            state = ActionState.Idle;
            return BT_Node.Status.Failure;
        }
        else if (Vector3.Distance(destination, transform.position) < 2)
        {
            state = ActionState.Idle;
            return BT_Node.Status.Success;
        }

        return BT_Node.Status.Running;
    }

    // public BT_Node.Status GoToDoor(Transform door)
    // {
    //     BT_Node.Status s = GoToLocation(door.position);
    //
    //     if (s == BT_Node.Status.Success)
    //     {
    //         if (true)
    //         {
    //             door.gameObject.SetActive(false);
    //             return BT_Node.Status.Success;
    //         }
    //         else
    //         {
    //             return BT_Node.Status.Failure;
    //         }
    //     }
    //     else
    //     {
    //         return s;
    //     }
    //
    //
    // }
    //
    // public BT_Node.Status GetToItem()
    // {
    //     BT_Node.Status s = GoToLocation(target.position);
    //
    //     if (s == BT_Node.Status.Success)
    //     {
    //         target.parent = transform;
    //     }
    //
    //     return s;
    // }
    //
    // public BT_Node.Status GetToSafety()
    // {
    //     return GoToLocation(escapeRoute.position);
    // }
    //
    // public BT_Node.Status GoToFrontDoor()
    // {
    //     return GoToDoor(frontDoor);
    // }
    //
    // public BT_Node.Status GoToBackDoor()
    // {
    //     return GoToDoor(backDoor);
    // }
    //
    // public BT_Node.Status CanGetToItem()
    // {
    //     if (itemIsGuarded)
    //         return BT_Node.Status.Failure;
    //
    //     return BT_Node.Status.Success;
    // }

    public BT_Node.Status HasWork()
    {
        return _gameManager.workTime ? BT_Node.Status.Success : BT_Node.Status.Failure;
    }

    public BT_Node.Status GoToRest()
    {
        return GoToLocation(restPosition.position);
    }
    
    public BT_Node.Status HasOrder()
    {
        if (!_gameManager.hasOrder) return BT_Node.Status.Failure;
        
        _currentRecipe = _gameManager.CurrentRecipe;
        return BT_Node.Status.Success;
    }

    public BT_Node.Status HasResources()
    {
        return HasEnough() ? BT_Node.Status.Success : GoDeposit();
    }

    private bool HasEnough()
    {
        var requiredR = 0;
        var requiredB = 0;
        var requiredY = 0;
        
        
        // when ingredients > 2 needs changes
        switch (_currentRecipe.Ingredient1)
        {
            case IngredientType.Red:
                requiredR++;
                break;
            case IngredientType.Blue:
                requiredB++;
                break;
            case IngredientType.Yellow:
                requiredY++;
                break;
        }
        
        switch (_currentRecipe.Ingredient2)
        {
            case IngredientType.Red:
                requiredR++;
                break;
            case IngredientType.Blue:
                requiredB++;
                break;
            case IngredientType.Yellow:
                requiredY++;
                break;
        }

        if (_redIngredient - requiredR < 0)
        {
            _missingRed = true;
        }
        if (_blueIngredient - requiredB < 0)
        {
            _missingBlue = true;
        }
        if (_yellowIngredient - requiredY < 0)
        {
            _missingYellow = true;
        }

        return !_missingRed && !_missingBlue && !_missingYellow;
    }

    private BT_Node.Status GoDeposit()
    {
        if (_missingRed)
        {
            var temp = GoToLocation(redDepositPosition.position);
            if (temp == BT_Node.Status.Success)
            {
                _missingRed = false;
                _redIngredient = 5;
                AssignValue();
            }
            else
            {
                return BT_Node.Status.Running;
            }
        }
        else if (_missingBlue)
        {
            var temp = GoToLocation(blueDepositPosition.position);
            if (temp == BT_Node.Status.Success)
            {
                _missingBlue = false;
                _blueIngredient = 5;
                AssignValue();
            }
            else
            {
                return BT_Node.Status.Running;
            }

        }
        else if (_missingYellow)
        {
            var temp = GoToLocation(yellowDepositPosition.position);
            if (temp == BT_Node.Status.Success)
            {
                _missingYellow = false;
                _yellowIngredient = 5;
                AssignValue();
            }
            else
            {
                return BT_Node.Status.Running;
            }

        }

        if (!_missingRed && !_missingBlue && !_missingYellow)
        {
            Debug.Log("not miss");
            return BT_Node.Status.Success;
        }

        return BT_Node.Status.Running;
    }

    private BT_Node.Status GoToTable()
    {
        return GoToLocation(tablePosition.position);
    }


    private bool _craftFinished = false;
    private Coroutine _crafting;
    private BT_Node.Status Craft()
    {
        _crafting ??= StartCoroutine(Crafting());

        if (!_craftFinished) return BT_Node.Status.Running;
        
        _craftFinished = false;
        _crafting = null;
        return BT_Node.Status.Success;
    }

    private IEnumerator Crafting()
    {
        yield return new WaitForSeconds(2.0f);
        switch (_currentRecipe.Ingredient1)
        {
            case IngredientType.Red:
                _redIngredient--;
                break;
            case IngredientType.Blue:
                _blueIngredient--;
                break;
            case IngredientType.Yellow:
                _yellowIngredient--;
                break;
        }

        switch (_currentRecipe.Ingredient2)
        {
            case IngredientType.Red:
                _redIngredient--;
                break;
            case IngredientType.Blue:
                _blueIngredient--;
                break;
            case IngredientType.Yellow:
                _yellowIngredient--;
                break;
        }

        
        _craftFinished = true;
        pot.SetActive(true);
        AssignValue();
    }

    private BT_Node.Status GoToOutput()
    {
        var temp = GoToLocation(outPutPosition.position);
        if (temp != BT_Node.Status.Success) return temp;
        
        _currentRecipe = null;
        _gameManager.OrderReceived();
        pot.SetActive(false);
        return temp;
    }
}
