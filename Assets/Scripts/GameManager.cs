using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    [SerializeField] private float workDayDuration = 30.0f;
    [SerializeField] private Light directionalLight;
    [SerializeField] private TMP_Text text;
    [SerializeField] private TMP_Text round;
    [SerializeField] private Image ingredient1_img;
    [SerializeField] private Image ingredient2_img;
    
    public static GameManager Instance { get; private set; }
    public Recipe CurrentRecipe;
    public bool workTime = false;
    public bool hasOrder = false;

    
    private float _totalTime = 20;
    private float _currentTime = 0;
    private int _round;
    public int Round => _round;

    private void Awake()
    {
        Instance = this;
        _round = 0;
    }

    private void Start()
    {
        CurrentRecipe = GenerateOrder();
    }

    private void Update()
    {
        _totalTime += Time.deltaTime;
        _currentTime = _totalTime % workDayDuration;

        workTime = 0.0f <= GetHour() && GetHour() <= 8.0f ? false : true;
        directionalLight.intensity = workTime ? 2.0f : 0.01f;
        
        text.SetText(Clock24Hour());
    }
    
    
    public void OrderReceived()
    {
        CurrentRecipe = GenerateOrder();
        _round++;
        round.SetText("Round: " + _round);
    }

    private Recipe GenerateOrder()
    {
        var temp = new Recipe
        {
            Ingredient1 = Random.value switch
            {
                <= 0.33f => IngredientType.Red,
                >= 0.66f => IngredientType.Yellow,
                _ => IngredientType.Blue
            },
            Ingredient2 = Random.value switch
            {
                <= 0.33f => IngredientType.Red,
                >= 0.66f => IngredientType.Yellow,
                _ => IngredientType.Blue
            }
        };

        ingredient1_img.color = temp.Ingredient1 switch
        {
            IngredientType.Red => Color.HSVToRGB(0, 0.77f, 1),
            IngredientType.Blue => Color.HSVToRGB(2f / 3f, 0.77f, 1),
            IngredientType.Yellow => Color.HSVToRGB(1f / 6f, 0.77f, 1),
            _ => Color.white
        };
        
        ingredient2_img.color = temp.Ingredient2 switch
        {
            IngredientType.Red => Color.HSVToRGB(0, 0.77f, 1),
            IngredientType.Blue => Color.HSVToRGB(2f / 3f, 0.77f, 1),
            IngredientType.Yellow => Color.HSVToRGB(1f / 6f, 0.77f, 1),
            _ => Color.white
        };
        
        return temp;
    }

    #region TimeManagement

    
    public float GetHour()
    {
        return _currentTime * 24.0f / workDayDuration;
    }

    public float GetMinutes()
    {
        return (_currentTime * 24.0f * 60.0f / workDayDuration)% 60.0f;
    }

    public string Clock24Hour()
    {
        //00:00
        return Mathf.FloorToInt(GetHour()).ToString("00") + ":" + Mathf.FloorToInt(GetMinutes()).ToString("00");
    }

    #endregion
    
    
}
